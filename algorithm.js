/**
 * Random select algorithm
 */

// swap two numbers
function swap(arr, left_idx, right_idx) {
    if (arr[left_idx] != arr[right_idx]){
        let temp = arr[left_idx];
        arr[left_idx] = arr[right_idx];
        arr[right_idx] = temp;
    }
}

// original partition
function partition(A,p,r) {
    let x = A[r];
    let i = p - 1;
    for (let j = p; j < r; j++) {
        if (A[j]<=x) {
            i++;
            swap(A, i, j)
        };
    }
    swap(A, i + 1, r);
    return i + 1;
}

function kth_partition(A, p, r, s) {
    for (let index = 0; index < r; index++) {
        if (s == A[index])
            swap(A, index, r);
    }
    var x = A[r];
    var i = p - 1;
    for (let j = p; j < r; j++) {
        if (A[j] <= x) {
            i++;
            swap(A, i, j);
        } 
    }
    swap(A, i + 1, r);
    return i + 1;
}

function random_partition(A,p,r) {
    var f = Math.floor(Math.random() * (r - p + 1)) + p;
    swap(A, f, r);
    return partition(A, p, r);
}

function random_select(A, p, r, i) {
    if (p == r){
        return A[p];
    }
    q = random_partition(A, p, r);
    k = q - p + 1;
    if (i == k){
        return A[q];
    }
    else if (i < k)
        return random_select(A, p, q - 1, i);
    else return random_select(A, q + 1, r, i - k);
}

function insert_sort(A, p, r, k) {
    var i, j, key;
    for (let i = p+1; i <= r; i++){
        key = A[i];
        j = i - 1;
        while (j >= p && A[j] > key) {
            A[j + 1] = A[j];
            --j;
        }
        A[j + 1] = key;
    }
    return A[p + k - 1]; 
}

function select(A, p, r, i, group) {
    if (p == r)
        return A[p];
    var n = A.length;
    var n2 = ceil(n / group);//中位数组的长度，ceil是上取整
    var groupMedian = ceil(group / 2);// 子数组的中位数
    var newArr;//新建一个二维数组；
    var Arr;//中位数的集合的数组；
    for (let i = 0; i < n2; i++) {
        Arr[i] = newArr[i][groupMedian];
    }
    var cur = select(Arr, 0, n2, ceil(n2 / 2), group);
    var j = kth_partition(A, 0, n, cur);

    if (m == k)
        return A[j]
    else if (m < k)
        return select(A[0..m - 1(A数组从0到m)], 0, m - 1, k, group);
    else
        return select(A[m + 1..r], 0, r - m + 1, group);
}
// median = select(A, p, r, A.length / 2, group);

// original quicksort finished
function quicksort(A, p, r) {
    if (p < r) {
        q = partition(A, p, r);
        quicksort(A, p, q - 1)
        quicksort(A, q + 1, r)
    }
    return A
}

// 输入找到的kth作为partition.
function quicksort_kth(A, p, r, median) {
    if (p < r) {
        q = kth_partition(A, p, r, median);
        quicksort(A, p, q - 1);
        quicksort(A, q + 1, r);
    }
    return A
}
